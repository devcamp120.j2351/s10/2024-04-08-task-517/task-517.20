import { Component } from "react";

class Info extends Component{
    constructor(props) {
        super(props);
        this.state = {
            soLuotDem: 10
        }
        //this.onButtonBamVaoDayClick = this.onButtonBamVaoDayClick.bind(this);

    }
    // Xử lý sự kiện khi bấm nút
    onButtonBamVaoDayClick = () => {
        this.setState({
            soLuotDem: this.state.soLuotDem + 1
        })
    }

    render () {
        return (
        <div>
            <button onClick = {this.onButtonBamVaoDayClick}>Bấm vào đây</button>
            <p>Số lượt đếm: {this.state.soLuotDem}</p>
        </div>
        )
    }
}

export default Info;